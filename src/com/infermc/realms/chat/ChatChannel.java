package com.infermc.realms.chat;

import com.infermc.realms.ranks.RealmRank;

/**
 * Created by Thomas on 23/01/2016.
 */
public class ChatChannel {
    private String name;
    private String format;
    private RealmRank talkRank; // Min rank required to talk
    private String command; // Command required to toggle this chat.
}
