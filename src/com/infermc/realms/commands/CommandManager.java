package com.infermc.realms.commands;

import com.infermc.realms.Realm;
import com.infermc.realms.RealmCore;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;

public class CommandManager {
    private RealmCore core;
    private HashMap<Player,InteractiveCommand> currentCommand = new HashMap<Player, InteractiveCommand>();

    public CommandManager(RealmCore c) {
        this.core = c;
    }
    public InteractiveCommand getCurrentCommand(Player p) {
        if (currentCommand.containsKey(p)) {
            return currentCommand.get(p);
        }
        return null;
    }
    public void setCurrentCommand(Player p, InteractiveCommand cmd) {
        if (cmd == null) {
            if (currentCommand.containsKey(p)) currentCommand.remove(p);
        } else {
            currentCommand.put(p, cmd);
        }
    }
    public InteractiveCommand createCommand(CommandCallable cb) {
        return new InteractiveCommand(this,cb);
    }
}
