package com.infermc.realms.commands;

import java.util.ArrayList;
import java.util.List;

public class InteractiveCommand {
    private CommandManager manager;
    private ChatQuestion currentQuestion;
    private List<ChatQuestion> questions = new ArrayList<ChatQuestion>();
    private CommandCallable callback;

    public InteractiveCommand(CommandManager mgr,CommandCallable cb) {
        this.callback = cb;
        this.manager = mgr;
        cb.cmd = this;
    }



    public List<ChatQuestion> getQuestions() {
        return questions;
    }
    public ChatQuestion getCurrentQuestion() {
        return this.currentQuestion;
    }
    public ChatQuestion getQuestion(String name) {
        for (ChatQuestion q : questions) {
            if (q.getName().equalsIgnoreCase(name)) return q;
        }

        return null;
    }

    public ChatQuestion getNext() {
        int cid = 0;
        if (questions.contains(this.currentQuestion)) {
            cid = questions.indexOf(currentQuestion);
        }
        if (cid+1 >= questions.size()) {
            return null;
        }
        return questions.get(cid+1);
    }
    public void goNext() {
        if (this.getNext() != null) {
            this.currentQuestion = this.getNext();
        } else {
            this.currentQuestion = null;
            try {
                this.callback.run();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void addQuestion(ChatQuestion question) {
        if (questions.size() == 0) currentQuestion = question;
        questions.add(question);
    }
}
