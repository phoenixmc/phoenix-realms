package com.infermc.realms.commands;

public class ChatQuestion {
    private String name;
    private String question;
    private String answer = null;

    public ChatQuestion(String name, String question) {
        this.name = name;
        this.question = question;
    }

    // Getters
    public String getAnswer() {
        return this.answer;
    }
    public String getName() {
        return this.name;
    }
    public String getQuestion() {
        return this.question;
    }

    // Setters
    public void setAnswer(String answer) {
        this.answer = answer;
    }
    public void setAnswer(String[] answer) {
        String answr = "";
        for (String str : answer) {
            answr = answr+" "+str;
        }
        setAnswer(answr.trim());
    }
}
