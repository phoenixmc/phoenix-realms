package com.infermc.realms.menu;

import org.bukkit.entity.Player;

import java.util.HashMap;

public class MenuManager {
    private HashMap<Player,Menu> currentMenu = new HashMap<Player, Menu>();

    public Menu createMenu() {
        return new Menu();
    }
    public Menu getCurrentMenu(Player p) {
        if (currentMenu.containsKey(p)) {
            return currentMenu.get(p);
        }
        return null;
    }
    public void setCurrentMenu(Player p, Menu m) {
        if (m == null) {
            currentMenu.remove(p);
        } else {
            currentMenu.put(p, m);
        }
    }
    public void closeCurrentMenu(Player p) {
        Menu menu = getCurrentMenu(p);
        if (menu != null) {
            if (p.getOpenInventory().getTopInventory().equals(menu.getInventory())) {
                p.closeInventory();
            }
        }
    }
}
