package com.infermc.realms.menu;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.HashMap;
import java.util.Map;

public class Menu {
    private Inventory inv = null;
    private Server server;
    private HashMap<Integer,MenuItem> items = new HashMap<Integer,MenuItem>();
    private int size = 54;

    public void generate() {
        Inventory inventory = Bukkit.createInventory(null,size);
        for (Map.Entry<Integer,MenuItem> item : items.entrySet()) {
            if (item.getKey() <= this.size) {
                inventory.setItem(item.getKey().intValue(), item.getValue().getStack());
            }
        }
        this.inv = inventory;
    }
    public Inventory getInventory() {
        if (this.inv == null) generate();
        return this.inv;
    }
    public void itemClick(Player player, Integer slot) {
        if (items.containsKey(slot)) {
            player.performCommand(items.get(slot).getCommand());
        }
    }
    public void setItem(Integer slot, MenuItem item) {
        items.put(slot,item);
    }
}
