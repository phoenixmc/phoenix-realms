package com.infermc.realms.menu;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thomas on 25/01/2016.
 */
public class MenuItem {
    private String title = "";
    private List<String> description = new ArrayList<String>();
    private String command = "";
    private Material icon;
    private ItemStack stack;

    public MenuItem(Material icon) {
        this.icon = icon;
        generate();
    }
    public MenuItem(Material icon, String title) {
        this.icon = icon;
        this.title = title;
        generate();
    }
    public MenuItem(Material icon, String title, List<String> desc) {
        this.icon = icon;
        this.title = title;
        this.description = desc;
        generate();
    }

    public String getTitle() {
        return this.title;
    }
    public List<String> getDescription() {
        return this.description;
    }
    public String getCommand() {
        return this.command;
    }
    public ItemStack getStack() {
        return this.stack;
    }
    public void generate() {
        ItemStack stack = new ItemStack(this.icon,1);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(title);
        meta.setLore(description);
        stack.setItemMeta(meta);
        this.stack = stack;
    }

    public void setCommand(String command) {
        this.command = command;
    }
}
