package com.infermc.realms;

import com.infermc.realms.ranks.RealmRank;
import org.bukkit.OfflinePlayer;

// Realm world chunk
public class WorldChunk {
    private float x;
    private float z;
    private RealmWorld world;

    private OfflinePlayer owner; // Who created the chunk, can also be used for a protected plot system
    private RealmRank buildRank; // Min rank required to build in this chunk - Useful for spawn protection
}
