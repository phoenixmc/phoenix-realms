package com.infermc.realms;

import com.infermc.realms.ranks.RealmRank;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RealmWorld {
    private RealmManager realmManager;
    private Realm parent;
    private World world;
    private String name;
    private String description;
    private int defaultGamemode;
    private File path;
    private boolean globalChat = true;

    // Min rank required to build.
    private RealmRank buildRank;
    private String template = "none";
    private long seed;
    private int slotIncrement = 10; // How by how many does this increase the max player slots in a realm?

    public RealmWorld(Realm parent, String name) {
        this.name = name;
        this.parent = parent;
        this.realmManager = parent.getRealmManager();
        this.buildRank = realmManager.getCore().getRankManager().getRank("player");
        this.path = new File(realmManager.getServer().getWorldContainer()+"/"+parent.getName()+"_"+name);
        if (!this.path.exists()) this.path.mkdirs();
    }
    public Realm getRealm() {
        return this.parent;
    }

    // World
    public void unloadWorld() {
        if (this.realmManager.getServer().getWorld(this.parent.getName()+"_"+this.name) == null) {
            this.realmManager.getServer().unloadWorld(this.world, true);
        }
        this.world = null;
    }
    public void loadWorld() {
        WorldCreator wc = new WorldCreator(this.parent.getName()+"_"+this.name);
        try {
            this.world = this.realmManager.getServer().createWorld(wc);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }
    public boolean isLoaded() {
        if (this.realmManager.getServer().getWorld(this.parent.getName()+"_"+this.name) == null) return false;
        return true;
    }

    // Data File
    public void saveData() {
        File dataFile = new File(this.path+"/world.yml");
        YamlConfiguration yaml = new YamlConfiguration();

        yaml.set("name",this.name);
        yaml.set("description",this.description);
        yaml.set("defaultGamemode",this.defaultGamemode);
        yaml.set("buildRank",this.buildRank.getName());
        yaml.set("template",this.template);
        yaml.set("seed",this.seed);
        yaml.set("slotIncrement",this.slotIncrement);

        try {
            yaml.save(dataFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public boolean loadData() {
        File dataFile = new File(this.path+"/world.yml");
        YamlConfiguration yaml = new YamlConfiguration();

        if (!dataFile.exists()) return false;

        try {
            yaml.load(dataFile);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
            return false;
        }

        if (yaml.contains("name")) {
            this.name = yaml.getString("name");
        } else {
            return false;
        }

        if (yaml.contains("description")) {
            this.description = yaml.getString("description");
        }

        if (yaml.contains("defaultGamemode")) {
            this.defaultGamemode = yaml.getInt("defaultGamemode");
        } else {
            return false;
        }

        if (yaml.contains("buildRank")) {
            this.buildRank = this.realmManager.getCore().getRankManager().getRank(yaml.getString("buildRank"));
        } else {
            return false;
        }

        if (yaml.contains("template")) {
            this.template = yaml.getString("template");
        }
        if (yaml.contains("seed")) {
            this.seed = yaml.getLong("seed");
        }
        if (yaml.contains("slotIncrement")) {
            this.slotIncrement = yaml.getInt("slotIncrement");
        }

        return true;
    }
    public void save() {
        if (isLoaded() && this.world != null) {
            this.world.save();
        }
    }
    // world is null if the world its self isnt actually loaded.
    public World getWorld() {
        return this.world;
    }

    // Spawn Handler
    public void setSpawn(Location newSpawn) {
        this.world.setSpawnLocation(newSpawn.getBlockX(),newSpawn.getBlockY(),newSpawn.getBlockZ());
    }
    public Location getSpawn() {
        return this.world.getSpawnLocation();
    }

    // Slots
    public int getSlotIncrement() {
        return this.slotIncrement;
    }
    public void setSlotIncrement(int increment) {
        this.slotIncrement = increment;
    }

    // Players
    public List<Player> getPlayers() {
        if (this.world != null) return this.world.getPlayers();
        return new ArrayList<Player>();
    }

    // Name
    public String getName() { return this.name; }

    // Path
    public File getPath() { return this.path; }

    // Description
    public String getDescription() {
        return this.description;
    }
    public void setDescription(String desc) {
        this.description = description;
    }

    // Gamemode.
    public int getDefaultGamemode() {
        return this.defaultGamemode;
    }
    public void setDefaultGamemode(int gamemode) {
        this.defaultGamemode = gamemode;
    }

    // Template
    public String getTemplate() {
        return this.template;
    }
    public void setTemplate(String template) {
        this.template = template;
    }

    // World Seed
    public long getWorldSeed() {
        return this.seed;
    }
    public void setWorldSeed(long seed) {
        this.seed = seed;
    }

    // Default build rank
    public RealmRank getBuildRank() {
        return this.buildRank;
    }
    public void setBuildRank(RealmRank rank) {
        this.buildRank = rank;
    }

    // Global Chat
    public boolean hasGlobalChat() {
        return this.globalChat;
    }
    public void setGlobalChat(boolean chat) {
        this.globalChat = chat;
    }
}
