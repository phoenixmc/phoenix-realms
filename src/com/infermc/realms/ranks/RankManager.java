package com.infermc.realms.ranks;

import java.util.ArrayList;
import java.util.List;

public class RankManager {
    private List<RealmRank> ranks = new ArrayList<RealmRank>();

    public RankManager() {

    }

    public RealmRank getRank(String name) {
        for (RealmRank rank : this.ranks) {
            if (rank.getName().equalsIgnoreCase(name)) return rank;
        }
        return null;
    }
    public boolean addRank(RealmRank rank) {
        if (getRank(rank.getName()) == null) {
            ranks.add(rank);
            return true;
        }
        return false;
    }
}
