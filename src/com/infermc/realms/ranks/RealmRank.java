package com.infermc.realms.ranks;

import org.bukkit.inventory.ItemStack;

// Realm rank
public class RealmRank {
    private String name;
    private int level;
    private ItemStack rankIcon;

    // Can they override everything (namely staff)
    private boolean hasOverride = false;

    public RealmRank(String name, int lvl, boolean hasOverride) {
        this.name = name;
        this.level = lvl;
        this.hasOverride = hasOverride;
    }

    public String getName() {
        return this.name;
    }
    public int getLevel() {
        return this.level;
    }
    public boolean hasOverride() {
        return hasOverride;
    }
}
