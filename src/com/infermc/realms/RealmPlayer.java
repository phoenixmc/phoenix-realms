package com.infermc.realms;

import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.UUID;

// A realm player.
public class RealmPlayer {
    private Player player = null;
    private UUID uuid;
    private RealmManager manager;

    public RealmPlayer(RealmManager mgr,UUID uuid) {
        this.uuid = uuid;
        this.manager = mgr;
    }

    // Gets the realm they own
    public Realm getOwnedRealm() {
        for (Realm realm : manager.getRealms()) {
            if (realm.getOwner().equals(this)) return realm;
        }
        return null;
    }

    // Gets the realm they're currently in
    public Realm getRealm() {
        for (Realm realm : manager.getRealms()) {
            if (realm.getWorld(this.player.getWorld()) != null) return realm;
        }
        return null;
    }
    public RealmWorld getWorld() {
        return null;
    }
    public Location getLocation() {
        return null;
    }

    public Player getPlayer() {
        return this.player;
    }
    public void setPlayer(Player player) {
        this.player = player;
    }

    public UUID getUuid() {
        return this.uuid;
    }

    // Magical teleport stuff
    public void joinRealm(Realm realm) {
        if (realm.isBanned(this)) {
            this.player.sendMessage("Cannot join "+realm.getName()+", you're banned :(");
            return;
        }
        if (realm.isSuspended() && !realm.getOwner().equals(this)) {
            this.player.sendMessage("Cannot join "+realm.getName()+"! Realm has been suspended!");
            return;
        }
        if (realm.isWhitelistOnly() && !realm.isWhitelisted(this)) {
            this.player.sendMessage("Cannot join "+realm.getName()+", you are not whitelisted!");
            return;
        }
        if (realm.getWorlds().size() <= 0) {
            this.player.sendMessage("Cannot join "+realm.getName()+", realm has no worlds!");
            return;
        }
        if (realm.getDefaultWorld() != null) {
            if (!realm.getDefaultWorld().isLoaded()) {
                this.player.sendMessage("Please wait while the world is loaded, you'll be added to the world after.");
                realm.getDefaultWorld().loadWorld();
            }
            this.player.teleport(realm.getDefaultWorld().getWorld().getSpawnLocation());
            for (Player p : realm.getDefaultWorld().getWorld().getPlayers()) {
                p.sendMessage(this.player.getDisplayName() + " joined the world");
            }
        } else {
            this.player.sendMessage("Realm has no default world.");
        }
    }
}
