package com.infermc.realms;

import org.bukkit.OfflinePlayer;
import org.bukkit.Server;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

// Handles all things realm related - Doesn't include players
public class RealmManager {
    private RealmCore core;
    private Server server;
    private File dataDir;
    private File realmDir;
    private File playerDir;
    private List<Realm> realmList = new ArrayList<Realm>();
    private List<RealmPlayer> playerList = new ArrayList<RealmPlayer>();

    public RealmManager(RealmCore core,File dir) {
        this.core = core;
        this.dataDir = dir;
        this.playerDir = new File(dir+"/players");
        this.realmDir = new File(dir+"/realms");
        this.server = core.getServer();

        if (!this.dataDir.exists()) if (!this.dataDir.mkdirs()) getServer().getLogger().warning("Unable to create realm data directory.");
        if (!this.playerDir.exists()) if (!this.playerDir.mkdirs()) getServer().getLogger().warning("Unable to create realm players directory.");
        if (!this.realmDir.exists()) if (!this.realmDir.mkdirs()) getServer().getLogger().warning("Unable to create realms directory.");
    }
    public void savePlayers() {
        if (!playerDir.exists()) playerDir.mkdirs();
        for (RealmPlayer realmPlayer : playerList) {
            File playerFile = new File(playerDir+"/"+realmPlayer.getPlayer().getUniqueId().toString()+".yml");
            YamlConfiguration playerYaml = new YamlConfiguration();
            playerYaml.set("uuid",realmPlayer.getPlayer().getUniqueId().toString());

            try {
                playerYaml.save(playerFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public void loadPlayers() {
        // Loads all realms from disk.
        if (!playerDir.exists()) {
            server.getLogger().warning("Error making player dir!");
            if (!playerDir.mkdirs()) return;
        }
        if (playerDir.listFiles() != null) {
            server.getLogger().info("Loading Players");
            for (File file : playerDir.listFiles()) {
                if (!file.isDirectory()) {
                    // Possible realm?
                    if (file.exists()) {
                        server.getLogger().info("Loading player file "+file.getName());
                        YamlConfiguration playerYaml = new YamlConfiguration();
                        try {
                            playerYaml.load(file);
                        } catch (IOException e) {
                            server.getLogger().warning("Unable to load player file "+file.getName());
                            e.printStackTrace();
                            return;
                        } catch (InvalidConfigurationException e) {
                            server.getLogger().warning("Unable to load player file "+file.getName());
                            e.printStackTrace();
                            return;
                        }
                        UUID uuid = null;
                        if (playerYaml.contains("uuid")) {
                            uuid = UUID.fromString(playerYaml.getString("uuid"));
                        } else {
                            server.getLogger().warning("Unable to load player file "+file.getName());
                            return;
                        }

                        RealmPlayer realmPlayer = new RealmPlayer(this,uuid);
                        playerList.add(realmPlayer);
                        server.getLogger().info("Loaded player "+uuid);
                    }
                }
            }
        }
    }
    public void createPlayer(Player player) {
        RealmPlayer realmPlayer = new RealmPlayer(this,player.getUniqueId());
        realmPlayer.setPlayer(player);
    }
    public void registerPlayer(Player player) {
        for (RealmPlayer rp : this.playerList) {
            if (rp.getUuid().equals(player.getUniqueId())) {
                rp.setPlayer(player);
                return;
            }
        }
        RealmPlayer newPlayer = new RealmPlayer(this,player.getUniqueId());
        newPlayer.setPlayer(player);
        playerList.add(newPlayer);
    }
    public void loadRealms() {
        // Loads all realms from disk.
        if (!realmDir.exists()) {
            server.getLogger().warning("Error making realms dir!");
            if (!realmDir.mkdirs()) return;
        }
        if (realmDir.listFiles() != null) {
            server.getLogger().info("Loading Realms");
            for (File file : realmDir.listFiles()) {
                if (file.isDirectory()) {
                    // Possible realm?
                    server.getLogger().info(file.getName());
                    File realmYAML = new File(file+"/realm.yml");
                    if (realmYAML.exists()) {
                        server.getLogger().info("Loading realm "+file.getName());
                        Realm realm = new Realm(this,file);
                        if (realm.load()) {
                            server.getLogger().info("Loaded realm "+realm.getName());
                            this.realmList.add(realm);
                        } else {
                            server.getLogger().warning("Unable to load realm "+file.getName());
                        }
                    }
                }
            }
        }
    }
    public void saveAll() {
        saveRealms();
        savePlayers();
    }
    public void saveRealms() {
        for (Realm realm : realmList) {
            realm.saveWorlds();
            realm.save();
        }
    }

    public Server getServer() {
        return this.server;
    }
    public RealmCore getCore() { return this.core; }

    // Player stuff
    public RealmPlayer getPlayer(OfflinePlayer player) {
        for (RealmPlayer rPlayer : playerList) {
            if (rPlayer.getPlayer().equals(player)) return rPlayer;
        }
        return null;
    }
    public RealmPlayer getPlayer(UUID uuid) {
        for (RealmPlayer rPlayer : playerList) {
            if (rPlayer.getUuid().equals(uuid)) return rPlayer;
        }
        return null;
    }

    // Realm stuff
    public List<Realm> getRealms() {
        return this.realmList;
    }
    public Realm getRealm(String name) {
        for (Realm realm : realmList) {
            if (realm.getName().equalsIgnoreCase(name)) return realm;
        }
        return null;
    }
    public Realm createRealm(String name,OfflinePlayer owner) {
        if (getRealm(name) == null) {
            File realmPath = new File(realmDir+"/"+name);
            Realm realm = new Realm(this,realmPath);
            realm.setName(name);
            realm.setOwner(getPlayer(owner));
            realm.setRealmAddress(name.toLowerCase());
            realmList.add(realm);
            return realm;
        }
        return null;
    }
}
