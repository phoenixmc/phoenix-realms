package com.infermc.realms;

import com.infermc.realms.commands.ChatQuestion;
import com.infermc.realms.commands.CommandCallable;
import com.infermc.realms.commands.CommandManager;
import com.infermc.realms.commands.InteractiveCommand;
import com.infermc.realms.menu.Menu;
import com.infermc.realms.menu.MenuItem;
import com.infermc.realms.menu.MenuManager;
import com.infermc.realms.ranks.RankManager;
import com.infermc.realms.ranks.RealmRank;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.CachedServerIcon;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class RealmCore extends JavaPlugin implements Listener {
    private RealmManager manager;
    private RankManager rankManager;
    private CommandManager commandManager;
    private MenuManager menuManager;

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this,this);

        commandManager = new CommandManager(this);
        menuManager = new MenuManager();

        rankManager = new RankManager();
        RealmRank staff = new RealmRank("Staff",100,true);

        // Realm ranks
        RealmRank owner = new RealmRank("Owner",10,false);
        RealmRank builder = new RealmRank("Builder",5,false);
        RealmRank player = new RealmRank("Player",1,false);
        RealmRank guest = new RealmRank("Guest",0,false);

        rankManager.addRank(staff);
        rankManager.addRank(owner);
        rankManager.addRank(builder);
        rankManager.addRank(player);
        rankManager.addRank(guest);

        manager = new RealmManager(this,new File(getDataFolder()+"/../../PhoenixRealms"));
        manager.loadPlayers();
        manager.loadRealms();
    }

    public void onDisable() {
        manager.saveAll();
    }

    // Getters
    public RankManager getRankManager() { return this.rankManager; }
    public RealmManager getRealmManager() { return this.manager; }
    public MenuManager getMenuManager() { return this.menuManager; }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        String cmd = command.getName();
        Player player = null;
        if (sender instanceof Player) player = (Player) sender;

        if (player != null) {
            if (commandManager.getCurrentCommand(player) != null) {
                return true;
            }
        }

        if (cmd.equalsIgnoreCase("realm")) {
            if (args.length <= 0) {
                // Help info
            } else if (args[0].equalsIgnoreCase("create")) {
                if (player != null) {
                    RealmPlayer realmPlayer = manager.getPlayer(player);
                    if (realmPlayer.getOwnedRealm() == null) {
                        final Player finalPlayer = player;
                        InteractiveCommand iCMD = commandManager.createCommand(new CommandCallable() {
                            @Override
                            public void run() {
                                String realmName = cmd.getQuestion("name").getAnswer();
                                String realmAddress = cmd.getQuestion("address").getAnswer();
                                if (manager.getRealm(realmName) == null) {
                                    Realm realm = manager.createRealm(realmName,finalPlayer);
                                    realm.setRealmAddress(realmAddress);
                                    finalPlayer.sendMessage("Realm created!");
                                    realm.save();
                                } else {
                                    finalPlayer.sendMessage("A realm with that name already exists!");
                                }
                                menuManager.closeCurrentMenu(finalPlayer);
                            }
                        });
                        iCMD.addQuestion(new ChatQuestion("name","Please enter a name for your realm in chat!"));
                        iCMD.addQuestion(new ChatQuestion("address","Please enter an address for your realm, the address will end with .realm.p-n.ca"));
                        commandManager.setCurrentCommand(player,iCMD);
                        player.sendMessage(iCMD.getCurrentQuestion().getQuestion());
                    } else {
                        sender.sendMessage("You already own a realm!");
                    }
                }
            } else if (args[0].equalsIgnoreCase("join")) {
                if (args.length >= 2) {
                    Realm realm = manager.getRealm(args[1]);
                    RealmPlayer realmPlayer = manager.getPlayer(player);
                    if (realm == null) {
                        player.sendMessage("Realm does not exist!");
                    } else {
                        realmPlayer.joinRealm(realm);
                    }
                } else {
                    player.sendMessage("/realm join [name]");
                }
            }
            return true;
        } else if (cmd.equalsIgnoreCase("world")) {
            if (args.length <= 0) {

            } else if (args[0].equalsIgnoreCase("create")) {
                final RealmPlayer realmPlayer = manager.getPlayer(player);
                if (manager.getPlayer(player).getOwnedRealm() == null) {
                    player.sendMessage("You don't own a realm!");
                } else {
                    final Player finalPlayer = player;
                    InteractiveCommand iCMD = commandManager.createCommand(new CommandCallable() {
                        @Override
                        public void run() {
                            String worldName = cmd.getQuestion("name").getAnswer();
                            if (realmPlayer.getOwnedRealm().getWorld(worldName) == null) {
                                RealmWorld world = realmPlayer.getOwnedRealm().createWorld(worldName);
                                if (realmPlayer.getOwnedRealm().getDefaultWorld() == null) realmPlayer.getOwnedRealm().setDefaultWorld(world);
                                finalPlayer.sendMessage("World created!");
                            } else {
                                finalPlayer.sendMessage("A world with that name already exists!");
                            }
                            menuManager.closeCurrentMenu(finalPlayer);
                        }
                    });
                    iCMD.addQuestion(new ChatQuestion("name","What do you want to name your world?"));
                    commandManager.setCurrentCommand(player,iCMD);
                    player.sendMessage(iCMD.getCurrentQuestion().getQuestion());
                }
            } else if (args[0].equalsIgnoreCase("list")) {
                List<String> response = new ArrayList<String>();
                response.add("Worlds:");
                RealmPlayer realmPlayer = manager.getPlayer(player);
                for (RealmWorld world : realmPlayer.getOwnedRealm().getWorlds()) {
                    response.add(world.getName());
                }
                String str = "";
                for (String s : response) {
                    str = str + " " + s;
                }
                player.sendMessage(str.trim());
            }
        }
        return false;
    }

    @EventHandler
    public void onPing(ServerListPingEvent ev) {
        String prefix = getConfig().getString("addressPrefix","");
        String suffix = getConfig().getString("addressSuffix",".realm.p-n.ca");
        Realm realm = manager.getRealms().get(0);
        if (realm != null) {
            File realmPath = realm.getPath();
            File serverIcon = new File(realmPath + "/icon.png");
            if (serverIcon.exists()) {
                if (serverIcon.isFile()) {
                    CachedServerIcon icon = null;
                    try {
                        icon = getServer().loadServerIcon(serverIcon);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (icon != null) {
                        ev.setServerIcon(icon);
                    }
                }
            }
            ev.setMotd(realm.getName() + " - Phoenix Realms\n" + realm.getMotd());
            ev.setMaxPlayers(realm.getSlots());
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent ev) {
        ev.setJoinMessage(null);
        manager.registerPlayer(ev.getPlayer());

        for (Realm realm : manager.getRealms()) {
            if (realm.getOwner().getPlayer().equals(ev.getPlayer())) {
                ev.getPlayer().sendMessage("You own the realm "+realm.getName());
            } else {
                getLogger().info("Owner isnt "+realm.getOwner().getPlayer().getName());
            }
        }
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent ev) {
        if (commandManager.getCurrentCommand(ev.getPlayer()) != null) {
            InteractiveCommand cmd = commandManager.getCurrentCommand(ev.getPlayer());
            if (cmd.getCurrentQuestion() != null) {
                if (cmd.getCurrentQuestion().getAnswer() == null) {
                    cmd.getCurrentQuestion().setAnswer(ev.getMessage());
                    cmd.goNext();
                    if (cmd.getCurrentQuestion() != null) {
                        ev.getPlayer().sendMessage(cmd.getCurrentQuestion().getQuestion());
                    } else {
                        commandManager.setCurrentCommand(ev.getPlayer(),null);
                    }
                    ev.setCancelled(true);
                    return;
                }
            } else {
                commandManager.setCurrentCommand(ev.getPlayer(),null);
            }
        }
        Menu mnu = new Menu();
        MenuItem item = new MenuItem(Material.DIRT,"Create Realm");
        item.setCommand("realm create");
        mnu.setItem(1,item);
        menuManager.setCurrentMenu(ev.getPlayer(),mnu);
        ev.getPlayer().openInventory(mnu.getInventory());

        for (Player p : ev.getPlayer().getWorld().getPlayers()) {

            p.sendMessage("<("+ev.getPlayer().getWorld().getName()+")"+ev.getPlayer().getName()+"> "+ev.getMessage());
        }

        ev.setCancelled(true);
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent ev) {
        if (ev.isCancelled()) return;
        Player player = (Player) ev.getWhoClicked();
        if (menuManager.getCurrentMenu(player) != null) {
            Menu currentMenu = menuManager.getCurrentMenu(player);
            if (ev.getClickedInventory().equals(currentMenu.getInventory())) {
                currentMenu.itemClick(player, ev.getSlot());
                ev.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onInvClose(InventoryCloseEvent ev) {
        //menuManager.setCurrentMenu((Player) ev.getPlayer(),null);
    }
}
