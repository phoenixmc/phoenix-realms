package com.infermc.realms;

import com.infermc.realms.ranks.RealmRank;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.*;

// A realm!
public class Realm {
    private RealmManager realmManager;
    private String name;
    private String motd = "";
    private RealmPlayer owner;
    private int creationStamp;
    private RealmWorld defaultWorld = null;

    private File path;
    //private File worldDir;
    private File pluginsDir;

    private List<RealmWorld> worldList = new ArrayList<RealmWorld>();
    private List<RealmPlayer> banList = new ArrayList<RealmPlayer>();
    private List<RealmPlayer> whitelist = new ArrayList<RealmPlayer>();
    private boolean whitelistOnly = false;
    private boolean suspended = false;
    private int baseSlots = 10; // Max players allowed to join at once. (Doesn't include realm owner or staff)
    private String realmAddress;
    private HashMap<OfflinePlayer,RealmRank> ranks = new HashMap<OfflinePlayer, RealmRank>();

    public Realm(RealmManager realmManager,File path) {
        this.path = path;
        //this.worldDir = new File(path+"/worlds");
        this.pluginsDir = new File(path+"/plugins");

        //if (!this.worldDir.exists()) this.worldDir.mkdirs();
        if (!this.pluginsDir.exists()) this.pluginsDir.mkdirs();

        this.realmManager = realmManager;
        Date d = new Date();
        this.creationStamp = (int) d.getTime();
    }

    public RealmManager getRealmManager() {
        return this.realmManager;
    }

    // Loads the realm from the file
    public boolean load() {
        File config = new File(this.path+"/realm.yml");
        YamlConfiguration yamlConfig = new YamlConfiguration();
        try {
            yamlConfig.load(config);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
            return false;
        }

        // Load it up.
        if (yamlConfig.contains("name")) {
            this.name = yamlConfig.getString("name");
        } else {
            return false;
        }
        // Optional
        if (yamlConfig.contains("motd")) {
            this.motd = yamlConfig.getString("motd");
        }
        if (yamlConfig.contains("owner")) {
            this.owner = realmManager.getPlayer(UUID.fromString(yamlConfig.getString("owner")));
        } else {
            return false;
        }
        if (yamlConfig.contains("creationStamp")) {
            this.creationStamp = yamlConfig.getInt("creationStamp");
        } else {
            return false;
        }
        // optional
        if (yamlConfig.contains("banlist")) {
            List<RealmPlayer> blist = new ArrayList<RealmPlayer>();
            for (String uuid : (List<String>) yamlConfig.getList("banlist")) {
                blist.add(realmManager.getPlayer(UUID.fromString(uuid)));
            }
            this.banList = blist;
        }
        if (yamlConfig.contains("whitelist")) {
            List<RealmPlayer> wlist = new ArrayList<RealmPlayer>();
            for (String uuid : (List<String>) yamlConfig.getList("whitelist")) {
                wlist.add(realmManager.getPlayer(UUID.fromString(uuid)));
            }
            this.whitelist = wlist;
        }

        this.whitelistOnly = yamlConfig.getBoolean("whitelistOnly",false);
        this.suspended = yamlConfig.getBoolean("suspended",false);
        this.baseSlots = yamlConfig.getInt("slots",10);

        if (yamlConfig.contains("realmAddress")) {
            this.realmAddress = yamlConfig.getString("realmAddress");
        } else {
            return false;
        }

        if (yamlConfig.contains("worlds")) {
            List<String> worlds = yamlConfig.getStringList("worlds");
            for (String wName : worlds) {
                RealmWorld realmWorld = new RealmWorld(this,wName);
                if (realmWorld.loadData()) {
                    this.worldList.add(realmWorld);
                    this.getRealmManager().getServer().getLogger().warning("Loaded realm world "+wName);
                } else {
                    this.getRealmManager().getServer().getLogger().warning("Unable to load realm world "+wName);
                }
            }
        }
        if (yamlConfig.contains("defaultWorld")) {
            RealmWorld world = getWorld(yamlConfig.getString("defaultWorld"));
            if (world != null) this.defaultWorld = world;
        }
        return true;
    }
    public boolean loadWorlds() {
        // Kinda useless unless its to reload worlds from file.
        return true;
    }
   /*public File getWorldDir() {
        return this.worldDir;
    }
    */
    public File getPluginsDir() {
        return this.pluginsDir;
    }

    // Saves the realm to file
    public boolean save() {
        if (!this.path.exists()) this.path.mkdirs();
        File config = new File(this.path+"/realm.yml");
        YamlConfiguration yamlConfig = new YamlConfiguration();

        yamlConfig.set("name",this.name);
        yamlConfig.set("motd",this.motd);
        yamlConfig.set("owner",this.owner.getPlayer().getUniqueId().toString());
        yamlConfig.set("creationStamp",this.creationStamp);

        List<String> bans = new ArrayList<String>();
        for (RealmPlayer player : this.banList) {
            bans.add(player.getUuid().toString());
        }
        yamlConfig.set("banlist",bans);

        List<String> whitelisted = new ArrayList<String>();
        for (RealmPlayer player : this.whitelist) {
            whitelisted.add(player.getUuid().toString());
        }
        yamlConfig.set("whitelist",whitelisted);


        yamlConfig.set("whitelistOnly",this.whitelistOnly);
        yamlConfig.set("suspended",this.suspended);
        yamlConfig.set("slots",this.baseSlots);
        yamlConfig.set("realmAddress",this.realmAddress);

        List<String> worlds = new ArrayList<String>();
        for (RealmWorld world : this.worldList) {
            worlds.add(world.getName());
        }
        yamlConfig.set("worlds",worlds);
        if (this.defaultWorld != null) yamlConfig.set("defaultWorld",this.defaultWorld.getName());

        // Ranks whee.

        try {
            yamlConfig.save(config);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public void saveWorlds() {
        for (RealmWorld realmWorld: worldList) {
            realmWorld.saveData();
            realmWorld.save();
        }
    }
    // Creates a new realm.
    public boolean create(String name,OfflinePlayer owner) {
        return false;
    }

    public File getPath() {
        return this.path;
    }

    // Slots
    public int getSlots() {
        int slots = baseSlots;
        for (RealmWorld world : worldList) {
            slots += world.getSlotIncrement();
        }
        return slots;
    }
    public int getBaseSlots() {
        return this.baseSlots;
    }
    public void setBaseSlots(int slots) {
        this.baseSlots = slots;
    }
    public int getOnlineCount() {
        int count = 0;
        for (RealmWorld world : worldList) {
            count += world.getPlayers().size();
        }
        return count;
    }

    // Motd
    public String getMotd() {
        return this.motd;
    }
    public void setMotd(String motd) {
        this.motd = motd;
    }

    // Name
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }

    // Owner!
    public RealmPlayer getOwner() {
        return this.owner;
    }
    public void setOwner(RealmPlayer player) {
        this.owner = player;
    }

    // Suspended?
    public boolean isSuspended() {
        return this.suspended;
    }
    public void setSuspended(boolean suspended) {
        this.suspended = suspended;
    }

    // Realm Address
    public String getRealmAddress() {
        return this.realmAddress;
    }
    public void setRealmAddress(String realmAddress) {
        this.realmAddress = realmAddress;
    }

    // Lists
    public boolean isBanned(RealmPlayer player) {
        return banList.contains(player);
    }
    public boolean isWhitelisted(RealmPlayer player) {
        return whitelist.contains(player);
    }
    public boolean isWhitelistOnly() {
        return this.whitelistOnly;
    }

    // Worlds
    public List<RealmWorld> getWorlds() {
        return this.worldList;
    }
    public RealmWorld getWorld(String name) {
        for (RealmWorld world : this.worldList) {
            if (world.getName().equalsIgnoreCase(name)) return world;
        }
        return null;
    }
    public RealmWorld getWorld(World world) {
        // Useful to check if the current world belongs in this realm.
        for (RealmWorld w : this.worldList) {
            if (w.getWorld().equals(world)) return w;
        }
        return null;
    }
    public RealmWorld getDefaultWorld() {
        return this.defaultWorld;
    }
    public void setDefaultWorld(RealmWorld world) {
        this.defaultWorld = world;
    }
    public void deleteWorld(RealmWorld world) {
        for (Player p : world.getWorld().getPlayers()) {
            p.teleport(realmManager.getServer().getWorlds().get(0).getSpawnLocation());
            p.sendMessage("Kick from world, world has been deleted by realm owner.");
        }
        if (this.defaultWorld.equals(world)) this.defaultWorld = null;
        this.worldList.remove(world);
        world.getPath().delete();
    }
    public RealmWorld createWorld(String name) {
        if (this.getWorld(name) != null) return null;
        RealmWorld world = new RealmWorld(this,name);
        this.worldList.add(world);
        return world;
    }
}
